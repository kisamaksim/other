Данный артефакт дает сугубо личное сравнение двух книг по Java
http://aad.tpu.ru/1955/Java%20books/Core%20Java%20SE%209%20for%20the%20Impatient,%202nd%20Edition.pdf - Java SE 9 basic for impatient
https://sophia.javeriana.edu.co/~cbustaca/docencia/POO-2016-01/documentos/Thinking_in_Java_4th_edition.pdf - Thinking in java

Лучше сначала прочитать Thinking in java, а после нее взяться за Java SE 9 basic for impatient

# Key Point Thinking in Java
1. Ограничивается JAVA 5
2. Перед продчтением нужно прочитать, про паттерны Декоратор, Стратегия, Состояние
3. Книга дает более глубокое понимание основных конструкций Java, да и вообще о Java (в сравнении с Java SE 9 basic for impatient)
4. Во время чтения лучше иметь под рукой оригинал в браузере (иногда перевод мягко говоря сбивает с толку)


# Key Point Java SE 9 basic for impatient
1. Предоставляет основные конструкции и как ими пользоваться (и как ими стало возможно пользоваться в версии 9)
2. Не поясняет почему, просто предлагает пользоваться
3. Освещает такие моменты как лямбды, стримы, модули