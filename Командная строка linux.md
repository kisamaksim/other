# Командная строка Linux

Глава | Время начала | Время окончания | Всего часов
-|-|-|-
1| 21:30 | 21:42 | 12min
2| 21:42 | 21:52 | 10min
3| 21:52 | 22:30 | 38min
4| 22:30 | 23:16 | 46min
5| 22:25 | 23:01 | 36min
6| 17:15 | 17:50 | 45min
7| 18:00 | 18:45 | 45min
8| 19:00 | 19:33  | 33min
9| 21:10 | 22:27 | 1:17min
10| 11:15 | 12:00 | 45min
11| 12:00 | 13:15 | 1:15min
12| 13:20 | 14:45 | 1:25min
13| 16:35 | 17:12 | 37min
14| 17:15 | 17:50 | 35min
15| 18:00 | 18:30 | 30min
16| 19:21 | 20:18 | 1h
17| 11:15 |  |
18|  |  |
19|  |  |
20|  | 17:30 | 5h
21| 17:30 | 18:30 | 1h
22| 18:40 | 19:24 | 44min
23| 22:00 | 22:31 | 31min
24| 22:32 | 22:54 | 22min
25| 22:55 | 23:54 | 1h
26| 21:15 | 22:00 | 45min
27| 22:05 | 22:39 | 34min
28| 22:45 | 23:22 | 37min
29| 11:30 | 11:52 | 22min
30| 11:52 | 12:35 | 43min
31| 12:45 | 12:50 | 5min
32| 12:55 | 13:55 | 1h
33| 14:40 | 14:55 | 15min
34| 15:00 | 15:37 | 37min
35| 15:40 | 16:10 | 30min
36| 16:10 | 16:30 | 20min

Всего затрачено: 26 часов